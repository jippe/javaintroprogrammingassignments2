package section1_intro.part1_language_basics;


public class Point {
    int x;
    int y;

    /**
     * Create an instance of class point that is located at the same coordinates as the current object, but in the
     * diagonally opposing quadrant of coordinate space.
     * So, when the current point is at (4, 4), this method will return Point(-4, -4)
     * and when the current point is at (2, -5) it will return Point(-2, 5).
     * @return inverse Point
     */
    Point createInversePoint() {
        Point revPoint = new Point();
        revPoint.x = x*-1;
        revPoint.y = y*-1;

        return revPoint;
    }

    /**
     * This method returns the Euclidean distance of the current point (this) to the given point (otherPoint).
     * GIYF if you forgot what Euclidean distance is and how it is calculated.
     * @param otherPoint
     * @return euclidean distance
     */
    double euclideanDistanceTo(Point otherPoint) {
        int y2 = Math.abs (y - otherPoint.y);
        int x2 = Math.abs (x - otherPoint.x);
        double euclideanDistanceTo = Math.sqrt((Math.pow (y2, 2.0) + Math.pow(x2, 2.0)));
        return euclideanDistanceTo;
    }

    public static void main(String[] args) {
        Point point = new Point();
        System.out.println(point.x);
        System.out.println(point.createInversePoint());
    }
}
