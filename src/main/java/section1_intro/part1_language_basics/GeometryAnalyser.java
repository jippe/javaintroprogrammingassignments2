package section1_intro.part1_language_basics;

import java.text.NumberFormat;

public class GeometryAnalyser {
    public static void main(String[] args) {
        Point p1 = new Point();
        p1.x = Integer.parseInt(args[0]);
        p1.y = Integer.parseInt(args[1]);

        Point p2 = new Point();
        p2.x = Integer.parseInt(args[2]);
        p2.y = Integer.parseInt(args[3]);

        String method = args[4];
        if (method.equals("surf")){
            Rectangle rectangle =  new Rectangle();
            rectangle.upperLeft =  p1;
            rectangle.lowerRight =  p2;

            System.out.println(rectangle.getSurface());

        } else if (method.equals("dist")) {
            NumberFormat numberFormat = NumberFormat.getNumberInstance();
            numberFormat.setMaximumFractionDigits(1);

            System.out.println(numberFormat.format( p2.euclideanDistanceTo(p1) ));
        }
    }
}
