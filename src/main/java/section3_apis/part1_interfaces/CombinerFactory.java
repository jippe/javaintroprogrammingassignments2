package section3_apis.part1_interfaces;

public class CombinerFactory {
    /**
     * This method serves a StringCombiner that will surround the given arguments with double quotes,
     * separated by spaces and the result surrounded by single quotes.
     *
     * For example, the call
     *      combiner.combine("one", "two")
     * will return '"one" "two"'
     * @return quotedCombiner
     */
    static StringCombiner getQuotedCombiner() {
        return new StringCombiner() {
            @Override
            public String combine(String first, String second) {
                String quotedCombiner = "'" + "\"" + first + "\"" + " " + "\"" + second + "\"" + "'" ;
                return quotedCombiner;
            }
        };
    }

    /**
     * This method serves a StringCombiner that will combine the given arguments reversed and original,
     * concatenated with a space in between.
     *
     * For example,
     *      combiner.combine("one", "two")
     * will return "oneeno twoowt"
     *
     * (the quotes are not included in the return value).
     * @return reversedCombiner
     */
    static StringCombiner getReversedCombiner() {
        return new StringCombiner() {
            @Override
            public String combine(String first, String second) {
                String revFirst = new StringBuilder(first).reverse().toString();
                String revSecond = new StringBuilder(second).reverse().toString();
                String reversedCombiner = first+revFirst + " " + second+revSecond;
                return reversedCombiner ;
            }
        };
    }

    /**
     * <strong>Challenge!</strong>
     * This method serves a StringCombiner that will combine the given arguments so that the characters of both
     * arguments are converted to their ASCII values and then the summed. These numbers are combined with a space
     * in between and returned.
     *
     * For example,
     *      combiner.combine("one", "two")
     * will return "322 346"
     * Because 111 + 110 + 101 = 322 and 116 + 119 + 111 = 346.
     *
     * Hint: a char IS AN integer behind the scenes
     *
     * @return reversedCombiner
     */
    static StringCombiner getAsciiSumCombiner() {
        return new StringCombiner() {
            @Override
            public String combine(String first, String second) {
                int firstChar = 0;
                int secondAdd = 0;
                for (int i = 0; i < first.length(); i++){
                    firstChar += first.charAt(i);
                }
                for (int i = 0; i < second.length(); i++){
                    secondAdd += second.charAt(i);
                }
                return Integer.toString(firstChar) + " " + Integer.toString(secondAdd);
            }
        };
    }

}
