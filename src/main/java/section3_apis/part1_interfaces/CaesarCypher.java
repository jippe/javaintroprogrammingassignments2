package section3_apis.part1_interfaces;

import java.lang.reflect.Array;

public class CaesarCypher implements EncryptionEngine {
    

    @Override
    public String encrypt(String message) {
        char[] bs = message.toCharArray();
        for (int i = 0; i < bs.length; i++){
            if(!Character.toString(bs[i]).equals(" ")){
                bs[i] -= 3;
            }
        }
        String encryptedMessage = String.valueOf(bs);
        return encryptedMessage;
    }

    @Override
    public String decrypt(String encryptedMessage) {
        char[] zs = encryptedMessage.toCharArray();
        for (int i = 0; i < zs.length; i++){
            if(!Character.toString(zs[i]).equals(" ")){
                zs[i] += 3;
            }
        }
        String decryptedMessage = String.valueOf(zs);
        return decryptedMessage;
    }
}
